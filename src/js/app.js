import $ from "jquery";
import "popper.js";
import "bootstrap";

// Accordion scripts.
document.addEventListener(
	"DOMContentLoaded",
	function (event) {
		var acc   = document.getElementsByClassName( "accordion" );
		var panel = document.getElementsByClassName( "panel" );

		for (var i = 0; i < acc.length; i++) {
			acc[i].onclick = function () {
				var setClasses = ! this.classList.contains( "active" );
				setClass( acc, "active", "remove" );
				setClass( panel, "show", "remove" );

				if (setClasses) {
					this.classList.toggle( "active" );
					this.nextElementSibling.classList.toggle( "show" );
				}
			};
		}

		function setClass(els, className, fnName) {
			for (var i = 0; i < els.length; i++) {
				els[i].classList[fnName]( className );
			}
		}
	}
);


//If adding new classes to toggle, will most likely need to do so
//on #close-toggle function below this one:
jQuery( document ).ready(
	function ($) {
		$( "#search-toggle" ).on(
			"click",
			function () {
				$( "#search-toggle" ).toggleClass( "open" );
				$( ".searchform" ).toggleClass( "open" );
				$( ".right-menu" ).toggleClass( "open" );
				$( ".navbar-brand").toggleClass( "open" );
				$( ".toggler-menu").toggleClass( "open" );
				$( ".close-search").toggleClass( "open" );
				$( ".secondary-nav").toggleClass( "search-open" );
			}
		);
	}
);

//Close button (X icon / fa-times) that mimics above #search-toggle:
jQuery( document ).ready(
	function ($) {
		$( "#close-toggle" ).on(
			"click",
			function () {
				$( "#search-toggle" ).toggleClass( "open" );
				$( ".searchform" ).toggleClass( "open" );
				$( ".right-menu" ).toggleClass( "open" );
				$( ".navbar-brand").toggleClass( "open" );
				$( ".toggler-menu").toggleClass( "open" );
				$( ".close-search").toggleClass( "open" );
				$( ".secondary-nav").toggleClass( "search-open" );
			}
		);
	}
);

jQuery( document ).ready(
	function ($) {
		$( ".toggler-menu" ).on(
			"click",
			function () {
				$( this ).toggleClass( "close-label" );
			}
		);
	}
);

jQuery( document ).ready(
	function ($) {
		$( ".navbar-menu-toggler" ).click(
			function () {
				$( this ).next().toggleClass( "open" );
				$( this ).toggleClass( "rotate" );
			}
		);
	}
);

jQuery( document ).ready(
	function ($) {
		if( $( ".field--dynamic-block-fieldnode-subnav.bundle--type-page" ).find(".current_page_parent").length == 0) {
			$( ".field--dynamic-block-fieldnode-subnav.bundle--type-page" ).remove();
		}
	}
);

jQuery( document ).ready(
$("a").each(function () {
	var a = new RegExp("/" + window.location.host + "/");
	if (!a.test(this.href)) {
		$(this).attr("target", "_blank");
	}
})
);

// Toggle search with main-nav link
jQuery( document ).ready(
	function ($) {
		$( ".main-nav .nav-item-search, .lg-search-close" ).on(
			"click",
			function (e) {
				e.preventDefault();
				$('body').toggleClass('lg-search-active');
			}
		);
	}
);
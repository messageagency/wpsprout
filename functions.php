<?php

include 'inc/setup.php';

// START WP FUNCTIONS

function wpsprout_assets()
{
	wp_enqueue_style('theme-style', get_stylesheet_uri(), array('style'));
	wp_enqueue_style('style', get_stylesheet_directory_uri() . '/dist/css/style.css');
	wp_enqueue_script('js-file', get_stylesheet_directory_uri() . '/dist/js/app.js', array('jquery'), '1.0', true);
}

add_action('wp_enqueue_scripts', 'wpsprout_assets');

// Remove post type in generic permalink
function wpsprout_remove_cpt_slug($post_link, $post)
{
	if ('generic' === $post->post_type && 'publish' === $post->post_status) {
		$post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);
	}
	return $post_link;
}
add_filter('post_type_link', 'wpsprout_remove_cpt_slug', 10, 2);
function wpsprout_add_cpt_post_names_to_main_query($query)
{
	// Bail if this is not the main query.
	if (!$query->is_main_query()) {
		return;
	}
	// Bail if this query doesn't match our very specific rewrite rule.
	if (!isset($query->query['page']) || 2 !== count($query->query)) {
		return;
	}
	// Bail if we're not querying based on the post name.
	if (empty($query->query['name'])) {
		return;
	}
	// Add CPT to the list of post types WP will include when it queries based on the post name.
	$query->set('post_type', array('generic', 'news', 'event'));
}
add_action('pre_get_posts', 'wpsprout_add_cpt_post_names_to_main_query');

function add_custom_types_to_tax($query)
{
	if (is_category() || is_tag() && empty($query->query_vars['suppress_filters'])) {
		$post_types = get_post_types();
		$query->set('post_type', $post_types);
		return $query;
	}
}

// Remove default Posts type since no blog
// https://www.mitostudios.com/blog/how-to-remove-posts-blog-post-type-from-wordpress/
// Remove side menu
add_action('admin_menu', 'remove_default_post_type');

function remove_default_post_type()
{
	remove_menu_page('edit.php');
}

// Rename menu items
function rename_pages_menu_name()
{
	global $menu;
	foreach ($menu as $key => $item) {
		if ($item[0] === 'Pages') {
			$menu[$key][0] = __('Landing Page', 'textdomain');
		}
	}
	return false;
}
add_action('admin_menu', 'rename_pages_menu_name', 999);

// Remove comments from theme
// https://wordpress.stackexchange.com/questions/11222/is-there-any-way-to-remove-comments-function-and-section-totally
// Removes from admin menu
add_action('admin_menu', 'my_remove_admin_menus');
function my_remove_admin_menus()
{
	remove_menu_page('edit-comments.php');
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support()
{
	remove_post_type_support('post', 'comments');
	remove_post_type_support('page', 'comments');
}
// Removes from admin bar
function wpsprout_admin_bar_render()
{
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}
add_action('wp_before_admin_bar_render', 'wpsprout_admin_bar_render');

// Media Functions
// Add svg to allowed media
function wpsprout_mime_types($mimes)
{
	// New allowed mime types.
	$mimes['svg']  = 'image/svg';
	$mimes['svgz'] = 'image/svg+xml';
	$mimes['doc']  = 'application/msword';
	// Optional. Remove a mime type.
	unset($mimes['exe']);
	return $mimes;
}
add_filter('upload_mimes', 'wpsprout_mime_types');


// Query for attachments
// https://stackoverflow.com/questions/51796749/wp-advanced-custom-fields-wysiwyg-insert-link-to-media
add_filter('wp_link_query_args', 'link_query_args');



function link_query_args($query)
{
	$query['post_status'] = array('publish', 'inherit');
	$query['post_type']   = array('news', 'event', 'generic', 'resource', 'page', 'attachment');
	return $query;
}

// Link to media file URL instead of attachment page
add_filter('wp_link_query', 'link_query_results');


function link_query_results($results)
{
	foreach ($results as &$result) {
		if ('Media' === $result['info']) {
			$result['permalink'] = wp_get_attachment_url($result['ID']);
		}
	}
	return $results;
}


// Add Formats dropdown to wysiwyg
// Adding button styles, but can add other formatting options
// https://torquemag.io/2015/08/customize-wordpress-wysiwyg-editor/
function add_style_select_buttons($buttons)
{
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'add_style_select_buttons');

// Add Formats dropdown to wysiwyg
function wpsprout_custom_styles($init_array)
{

	$style_formats = array(
		// These are the custom styles
		array(
			'title'   => 'Primary Button',
			'block'   => 'a',
			'href'    => '#',
			'classes' => 'btn btn-primary',
			'wrapper' => true,
		),
		array(
			'title'   => 'Secondary Button',
			'block'   => 'a',
			'href'    => '#',
			'classes' => 'btn btn-secondary',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode($style_formats);
	return $init_array;
}
// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'wpsprout_custom_styles');

// Add Formats dropdown to wysiwyg
function wpsprout_block_formats($args)
{
	$args['block_formats'] = 'Heading=h2;Subheading=h3;Paragraph=p;';
	return $args;
}
add_filter('tiny_mce_before_init', 'wpsprout_block_formats');

// ACF update settings
function wpsprout_acf_init()
{

	acf_update_setting('google_api_key', 'AIzaSyDv_QESi3GMLk27aAJ9QSGJMm3ow2eHInQ');
}

add_action('acf/init', 'wpsprout_acf_init');


// Add custom image sizes to theme
add_image_size('banner', 1920, 1080, true);
add_image_size('billboard', 1920, 1080, true);
add_image_size('full', 1200, 800, false);
add_image_size('story', 1110, 912, true);
add_image_size('teaser', 528, 410, true);
add_image_size('tile', 720, 480, true);

// Remove items from admin bar
function wpsprout_remove_from_admin_bar($wp_admin_bar)
{
	$wp_admin_bar->remove_node('themes');
	$wp_admin_bar->remove_node('customize');
	$wp_admin_bar->remove_node('new-post');
	$wp_admin_bar->remove_node('new-listing_page');
	$wp_admin_bar->remove_node('new-event');
	$wp_admin_bar->remove_node('wpseo-menu');
}

add_action('admin_bar_menu', 'wpsprout_remove_from_admin_bar', 999);

// Add stylesheet to the editor
add_editor_style($stylesheet = 'dist/css/style.css');

// Move the SEO metabox to the bottom of the page
add_filter('wpseo_metabox_prio', function () {
	return 'low';
});


add_action('do_meta_boxes', 'wpsprout_image_metabox');
/**
 * Move Featured Image Metabox on 'rotator' post type
 * @author Bill Erickson
 * @link http://www.billerickson.net/code/move-featured-image-metabox
 */


function wpsprout_image_metabox()
{
	$image_metabox = ['page', 'event', 'news', 'generic'];
	remove_meta_box('postimagediv', $image_metabox, 'side');
	add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', $image_metabox, 'normal', 'high');
}

add_filter('rest_prepare_taxonomy', function ($response, $taxonomy, $request) {
	$context = !empty($request['context']) ? $request['context'] : 'view';
	// Context is edit in the editor
	if ($context === 'edit' && $taxonomy->meta_box_cb === false) {
		$data_response = $response->get_data();
		$data_response['visibility']['show_ui'] = false;
		$response->set_data($data_response);
	}
	return $response;
}, 10, 3);

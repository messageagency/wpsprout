/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. See https://github.com/JeffreyWay/laravel-mix.
 |
 */
const proxy = "https://wp-composer-base.test";
const mix = require("laravel-mix");

mix
	.sourceMaps(false, "source-map")
	.options({
		processCssUrls: false,
	})
	.webpackConfig({
		externals: {
			jquery: "jQuery",
		},
		module: {
			rules: [
				{
					test: /\.scss/,
					loader: "import-glob-loader",
				},
			],
		},
	})
	.setPublicPath("dist")
	.options({
		processCssUrls: false,
		cssNano: { minifyFontValues: false },
	});

/*
  |--------------------------------------------------------------------------
  | Fonts & Images
  |--------------------------------------------------------------------------
  */

mix.copy("src/fonts/**", "dist/fonts");

mix.copy("src/images/**", "dist/images");

/*
  |--------------------------------------------------------------------------
  | Browsersync
  |--------------------------------------------------------------------------
  */
mix.browserSync({
	proxy: proxy,
	files: [
		"./src/*",
		"./src/js/**/*.js",
		"./src/scss/**/*.scss",
		"./images/**/*.+(png|jpg|svg)",
		"./**/*.+(html|php)",
		"./templates/**/*.+(html|twig)",
	],
});

/*
  |--------------------------------------------------------------------------
  | SASS
  |--------------------------------------------------------------------------
  */
mix.sass("src/scss/style.scss", "dist/css");

/*
  |--------------------------------------------------------------------------
  | JS
  |--------------------------------------------------------------------------
  */
mix.js("src/js/app.js", "dist/js/");

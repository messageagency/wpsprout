<?php

/**
 * View container Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'view-container-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'view-container';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}

$context               = Timber::context();
$context['block']      = $block;
$context['field']      = get_fields();
$context['is_preview'] = $is_preview;

$band_view = get_field('band_view');

?>


<div class="band band--view-container full-bleed py-6">
	<div class="band__container container">
		<div class="text-center band__region band__region--band-header">
			<h2 class="band__heading mb-5 display-3 heading">
				<div class="field field--field-nand-heading bundle--type-view_container">
					<?php the_field('band_heading'); ?>
				</div>
			</h2>
		</div>
		<div class="band__region band__region--band-content">
			<div class="band__region band__region--band-content-inner">
				<div class="row">
					<?php
					$shortcode_filter = do_shortcode('[searchandfilter id="' . $band_view . '"]');
					if (!empty($shortcode_filter)) :
					?>
						<div class="col-12">
							<div class="filter--container">
								<?php echo $shortcode_filter; ?>
							</div>
						</div>
				  </div>
			<?php endif; ?>
				  <div class="results-container col-12">
					<?php echo do_shortcode('[searchandfilter id="' . $band_view . '" show="results"]'); ?>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
</div>

<?php

/**
 * Menu Grid Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'content-grid-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'content-grid';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

$context               = Timber::context();
$context['block']      = $block;
$context['field']      = get_fields();
$context['is_preview'] = $is_preview;

$overview = get_field('band_body');
?>


<div class="band band--content-grid full-bleed">
	<div class="band__container container">
		<div class="text-center band__region band__region--band-header">
			<h2 class="band__heading mb-5 display-3 heading">
				<div class="field field--field-pg-heading bundle--type-content_grid">
					<?php the_field('band_heading'); ?>
				</div>
			</h2>
			<?php if ($overview) : ?>
				<div class="band__lead lead">
					<div class="field field--field-pg-body bundle--type-content_grid">
						<?php the_field('band_body'); ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<div class="band__region band__region--band-content">
			<div class="band__region band__region--band-content-inner">
				<div class="field field--dynamic-block-fieldparagraph-grid-nav bundle--type-menu-grid">
					<div class="card-container card-container--grid card-deck">
						<?php
						$grid_posts = get_field('content_grid_items');
						if ($grid_posts) :
							foreach ($grid_posts as $grid_post) :
								$permalink = get_permalink($grid_post->ID);
								$title = get_the_title($grid_post->ID);
								$post_type = get_post_type($grid_post->ID);
								$remove = array('listing_page', 'generic', 'page');
								$post_type = str_replace($remove, '', $post_type);
								$date = get_the_date('F d, Y', $grid_post->ID);
								$image = get_the_post_thumbnail($grid_post->ID, 'tile', ['class' => 'img-fluid']);
								$event_date = get_field('event_date', $grid_post->ID);
								$resource_type = get_field('resource_type', $grid_post->ID);
								$news_type = get_field('news_type', $grid_post->ID);
								$location_type = get_field('location_type', $grid_post->ID);
								$profile_type = get_field('profile_type', $grid_post->ID);
						?>

<?php if (!empty($image)) : ?>
								<div class="card card--card-img-bg">
								<?php else : ?>
									<div class="card card--card-img-bg card--no-media">
									<?php endif; ?>
									<a href="<?php echo esc_url($permalink); ?>" class="card-inner">
										<?php

										if (!empty($image)) : ?>
											<div class="card-img-bg card-img">

												<?php
												echo $image;
												?>
											</div>
										<?php endif; ?>
										<?php if (!empty($image)) : ?>
										<div class="card-body card-img-overlay">
										<?php else : ?>
											<div class="card-body">
											<?php endif; ?>
											<div class="card-title">
												<?php if($post_type): ?>
													<div class="card__eyebrow eyebrow">
														<?php echo $post_type; ?>
													</div>
												<?php endif; ?>
												<h3 class="card__heading h4 heading"><?php echo esc_html($title); ?></h3>
												<div class="card__mustache mustache">
													<?php
													if ($news_type) {
														echo $news_type->name;
													};
													if ($resource_type) {
														echo $resource_type->name;
													}
													if ($profile_type) {
														echo $profile_type->name;
													}
													if ($location_type) {
														echo $location_type->name;
													}
													?>
												</div>
											</div>
										</div>
									</a>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>


		<div class="band__cta d-flex flex-column flex-md-row flex-wrap justify-content-center align-items-center mt-5">

			<?php
			$ctas = get_field('band_ctas');
			$cta_primary = false;
			$cta_secondary = false;
			if ($ctas) {
				$cta_primary = $ctas[0];
				$cta_primary_text = $cta_primary['band_ctas_link']['title'];
				$cta_primary_link = $cta_primary['band_ctas_link']['url'];
				if (!empty($ctas[1])) {
					$cta_secondary = $ctas[1];
					$cta_secondary_text = $cta_secondary['band_ctas_link']['title'];
					$cta_secondary_link = $cta_secondary['band_ctas_link']['url'];
				}
			}
			?>
			<?php if ($cta_primary) : ?>
				<div class="cta--primary">
					<a href="<?php echo $cta_primary_link; ?>" class="btn btn-primary"><?php echo $cta_primary_text; ?></a>
				</div>
			<?php endif; ?>
			<?php if ($cta_secondary) : ?>
				<div class="cta--secondary">
					<a href="<?php echo $cta_secondary_link; ?>" class="btn btn-primary"><?php echo $cta_secondary_text; ?></a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php

/**
 * Menu Grid Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'menu-grid-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'menu-grid';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}

$context               = Timber::context();
$context['post']       = new TimberPost($post_id);
$context['block']      = $block;
$context['field']      = get_fields();
$context['is_preview'] = $is_preview;
Timber::render( 'src/components/03-organisms/band/menu-grid.twig', $context );
